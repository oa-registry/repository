/**
 * Convert a date object to MySQL string.
 * @return {String}    this
 */
Date.prototype.toMySQL = function () {
  function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
  }
  return (
    this.getUTCFullYear() +
    "-" +
    twoDigits(1 + this.getUTCMonth()) +
    "-" +
    twoDigits(this.getUTCDate()) +
    " " +
    twoDigits(this.getUTCHours()) +
    ":" +
    twoDigits(this.getUTCMinutes()) +
    ":" +
    twoDigits(this.getUTCSeconds())
  );
};

/**
 * get week number
 * @return {Integer}    this
 */
Date.prototype.getWeek = function () {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return (
    1 +
    Math.round(
      ((date.getTime() - week1.getTime()) / 86400000 -
        3 +
        ((week1.getDay() + 6) % 7)) /
        7
    )
  );
};

Date.prototype.format = function (str) {
  var me = this;
  function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }
  var params = {
    MMMM: "<%A" + this.getMonth() + 1 + ">",
    MMM: "<%B" + this.getMonth() + 1 + ">",
    MM: pad(this.getMonth() + 1, 2),
    M: this.getMonth() + 1,
    dddd: "<%C" + this.getDay() + ">",
    ddd: "<%D" + this.getDay() + ">",
    dd: pad(this.getDate(), 2),
    d: this.getDate(),
    HH: pad(this.getHours(), 2),
    H: this.getHours(),
    hh: pad(this.getHours() % 12, 2),
    h: this.getHours() % 12,
    mm: pad(this.getMinutes(), 2),
    m: this.getMinutes(),
    ss: pad(this.getSeconds(), 2),
    s: this.getSeconds(),
    yyyy: this.getFullYear(),
    yy: this.getYear(),
    /*
    LT    : 15:09
    LTS   : 15:09:30
    L     : 22/07/2020
    l     : 22/7/2020
    */
    llll: (function () {
      return (
        "<%D" +
        me.getDay() +
        "> " +
        pad(me.getDate(), 2) +
        " <%B" +
        me.getMonth() +
        "> " +
        me.getFullYear() +
        " " +
        pad(me.getHours(), 2) +
        ":" +
        pad(me.getMinutes(), 2)
      );
    })(),
    LLLL: (function () {
      return (
        "<%C" +
        me.getDay() +
        "> " +
        pad(me.getDate(), 2) +
        " <%A" +
        me.getMonth() +
        "> " +
        me.getFullYear() +
        " " +
        pad(me.getHours() % 12, 2) +
        ":" +
        pad(me.getMinutes(), 2)
      );
    })(),
    LLL: (function () {
      return (
        pad(me.getDate(), 2) +
        " <%A" +
        me.getMonth() +
        "> " +
        me.getFullYear() +
        " " +
        pad(me.getHours() % 12, 2) +
        ":" +
        pad(me.getMinutes(), 2)
      );
    })(),
    lll: (function () {
      return (
        pad(me.getDate(), 2) +
        " <%B" +
        me.getMonth() +
        "> " +
        me.getFullYear() +
        " " +
        pad(me.getHours() % 12, 2) +
        ":" +
        pad(me.getMinutes(), 2)
      );
    })(),
    LL: (function () {
      return (
        pad(me.getDate(), 2) + " <%A" + me.getMonth() + "> " + me.getFullYear()
      );
    })(),
    ll: (function () {
      return (
        pad(me.getDate(), 2) + " <%B" + me.getMonth() + "> " + me.getFullYear()
      );
    })(),
    tt: (function () {
      if (me.getHours() < 12) return "<%E>";
      else return "<%F>";
    })(),
    t: (function () {
      if (me.getHours() < 12) return "<%G>";
      else return "<%H>";
    })(),
    "<": "<span class=",
    "%A": "i18n_MONTH_",
    "%B": "i18n_CMONTH_",
    "%C": "i18n_DAY_",
    "%D": "i18n_CDAY_",
    "%E": "i18n_HOUR_AM",
    "%F": "i18n_HOUR_PM",
    "%G": "i18n_HOUR_A",
    "%H": "i18n_HOUR_P",
    ">": "></span>",
  };
  for (var el in params) {
    var replace = el;
    var re = new RegExp(replace, "g");
    str = str.replace(re, params[el]);
  }
  return str;
};
