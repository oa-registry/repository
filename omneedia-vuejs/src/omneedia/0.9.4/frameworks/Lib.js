App.apply(App, {
  remote: "",
  APP: {},
  libs: [],
  namespace: Settings.NAMESPACE,
  controllers: [],
  /**
   * @method getArray
   * @param {Array} obj Array of objects
   * @param {String} field
   *
   * var o = [{field: "test"},{field: "test2"}];
   * var arr=getArray(o,'field');
   * result in ["test","test2"];
   * @return (Array) - flat array from an array of objects
   */
  getArray: function (obj, field) {
    var data = [];
    for (var i = 0; i < obj.length; i++) {
      data.push(obj[i][field]);
    }
    return data;
  },
});

/**
 * @namespace App
 * @class view
 * Implement the micro api method
 *
 */
App.define("App.fn", {
  statics: {
    defaultNS: "App.svc",
    items: [],
    add: function (item) {
      this.items.push(item);
    },
    process: function (ix, ndx, cb) {
      if (!ix[ndx]) return cb();

      function createfunc(uri, cls, FN, values) {
        var data = [];
        return function (...values) {
          for (var k = 0; k < values.length - 1; k++) {
            if (App.isJSON(values[k])) data.push(JSON.parse(values[k]));
            else data.push(values[k]);
          }
          var o = {
            action: cls.split(App.fn.defaultNS + ".")[1],
            method: FN,
            data: data,
            type: "rpc",
            tid: 1,
          };
          if (App.isFunction(values[values.length - 1]))
            var cb = values[values.length - 1];
          else throw "NO CALLBACK PROVIDED";

          App.request(
            {
              method: "POST",
              url: uri,
              data: o,
              headers: {
                "Content-Type": "application/json; charset=utf-8",
              },
            },
            function (e, r) {
              if (r) {
                r = JSON.parse(r);
                if (r.length > 0) r = r[0];
                if (r.result) var result = r.result;
                else var result = r.data;
                cb(null, result, r);
              } else cb(JSON.parse(e));
            }
          );
        };
      }

      for (var i in ix[ndx].actions) {
        if (i.indexOf("__QUERY__") > -1) var ns = "App.__QUERY__";
        else var ns = ix[ndx].namespace + "." + i.split(".js")[0];
        App.createNamespace(ns);
        for (var j = 0; j < ix[ndx].actions[i].length; j++) {
          var arguments = [];
          var fnt = createfunc(
            ix[ndx].url,
            ns,
            ix[ndx].actions[i][j].name,
            arguments
          );
          App.ns(ns)[ix[ndx].actions[i][j].name] = fnt;
        }
      }
      this.process(ix, ndx + 1, cb);
    },
    load(cb) {
      this.process(this.items, 0, cb);
    },
  },
});

/**
 * @namespace App
 * @class view
 * Implement the View Abstraction class (MVC)
 *
 */
App.define("App.view", {
  statics: {
    get: function (name) {
      return App.ns(App.namespace + ".view." + name);
    },
    define: function (name, o) {
      App.define(App.namespace + ".view." + name, { statics: o });
    },
    create: function (name) {},
    load: function (name, cb) {
      var components = [];

      var CSS = [];

      function loadStyle(s, ndx, cb) {
        if (!s[ndx]) return cb();
        function docss(css) {
          var t = css.split("url(");
          for (var i = 0; i < t.length; i++) {
            if (!Settings.DEBUG) t[i] += "url(";
            else t[i] += "url(/view/" + name + "/";
          }
          CSS.push(t.join(""));
          loadStyle(s, ndx + 1, cb);
        }
        var ext = s[ndx].substr(s[ndx].lastIndexOf("."), s[ndx].length);
        if (ext.indexOf(".css") > -1)
          App.require("/view/" + name + "/" + s[ndx], docss);
        else docss(s[ndx]);
      }
      function loadHTML(_html, cb) {
        function dohtml(html) {
          /** load styles */
          var styles = App.view.get(name).styles;
          var div = document.createElement("div");
          div.id = name;
          div.innerHTML = html;
          if (App.view.get(name).fullscreen) {
            div.style.height = "100%";
          }
          loadStyle(styles, 0, function () {
            var style = document.createElement("style");
            style.innerHTML = CSS.join("\n");
            div.appendChild(style);
            App._root.appendChild(div);
            if (App.view.get(name).show) App.view.get(name).show();
            if (App.view.get(name).controller) {
              var cz = App.view.get(name).controller;
              for (var el in cz) {
                components.push(el);
                var p = {
                  el: el,
                  data: {},
                  methods: {},
                  computed: {},
                };

                for (var elz in cz[el]) {
                  if (elz == "data") p.data = cz[el][elz];
                  else {
                    if (elz == "computed") p.computed[elz] = cz[el][elz];
                    else p.methods[elz] = cz[el][elz];
                  }
                }
                new Vue(p);
              }
            }
            setcontroller(App.controller.view[name], 0, cb);
          });
        }
        var ext = _html.substr(_html.lastIndexOf("."), _html.length);
        if (ext.indexOf(".htm") > -1)
          App.require("/view/" + name + "/" + App.view.get(name).html, dohtml);
        else dohtml(_html);
      }
      function setcontroller(cx, ndx, cb) {
        if (!cx[ndx]) {
          if (App.view.get(name).init) App.view.get(name).init();
          return cb();
        }
        var controller = cx[ndx];
        if (controller.controls) {
          for (var el in controller.controls) {
            if (el.indexOf(name) > -1) {
              var cz = controller.controls[el];
              if (components.indexOf(el.split(name)[1]) > -1)
                throw "Can't bind a component twice";
              var p = {
                el: el.split(name)[1],
                data: {},
                methods: {},
                computed: {},
              };
              components.push(el.split(name + " ")[1]);
              for (var el in cz) {
                if (el == "data") p.data = cz[el];
                else {
                  if (el == "computed") p.computed[el] = cz[el];
                  else p.methods[el] = cz[el];
                }
              }
              new Vue(p);
            }
          }
        }
        setcontroller(cx, ndx + 1, cb);
      }
      if (App.view.get(name)) {
        if (!App.view.get(name).html) throw "GURU MEDITATION: No HTML in view";
        /** load html */
        var _html = App.view.get(name).html;
        loadHTML(_html, cb);
      } else
        App.require("/view/" + name + "/" + name + ".js", function () {
          if (!App.view.get(name).html)
            throw "GURU MEDITATION: No HTML in view";

          /** load html */
          var _html = App.view.get(name).html;
          loadHTML(_html, cb);
        });
    },
  },
});

/**
 * @namespace App
 * @class controller
 * Implement the Controller Abstraction class (MVC)
 *
 */
App.define("App.controller", {
  statics: {
    view: {},
    define: function (name, o) {
      o.control = function (x) {};
      App.define(App.namespace + ".controller." + name, { statics: o });
      for (var i = 0; i < o.views.length; i++) {
        if (!this.view[o.views[i]]) this.view[o.views[i]] = [];
        this.view[o.views[i]].push(App.controller.get(name));
      }
    },
    get: function (name) {
      return App.ns(App.namespace + ".controller." + name);
    },
    create: function (name) {},
    load: function (name) {
      var me = this;
      if (App.controller.get(name)) {
        if (App.controller.get(name).init) App.controller.get(name).init();
        return;
      }
      App.require("/controller/" + name + ".js", function () {
        if (App.controller.get(name).init) App.controller.get(name).init();
      });
    },
  },
});

/*
 *
 * Fingerprint
 *
 */

App.AJAX.setDefaultHeaders({
  Authorization:
    "Bearer " + window.localStorage.getItem("socketcluster.authToken"),
});

GURU_MEDITATION_FATAL = false;
