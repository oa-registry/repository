/**
 * Loader module
 *
 */

App.apply(App, {
  init: function (o, fn) {
    App.__INIT__ = o;
    if (!App.key.get("app.udid"))
      App.key.set("app.udid", btoa(App.uuid() + "|" + navigator.userAgent));
    App.udid = App.key.get("app.udid");
    User = {};
    App.view.load(o, function () {
      App._kickstart(o, fn);
    });
  },
  init_langs: function () {
    for (var i = 0; i < Settings.LANGS.length; i++) {
      if (i == 0) var default_lang = Settings.LANGS[i].toUpperCase();
    }
    if (default_lang) {
      if (!window.localStorage.LANG) window.localStorage.LANG = default_lang;
    }
    if (window.localStorage.LANG) document.body.lang = window.localStorage.LANG;
  },
  _kickstart: function (o, fn) {
    var _p = this;
    if (window.Kickstart) {
      if (Kickstart.load) Kickstart.load();
      App._root.style.display = "block";
      App._root.classList.add("animated");
      App._root.classList.add("fadeIn");
    }
    if (fn) window.setTimeout(fn, 1000);
  },
  load: function (fn) {
    function lo(api, ndx, cb) {
      if (!api[ndx]) return cb();
      App.require("/fn/" + api[ndx] + ".js", function () {
        lo(api, ndx + 1, cb);
      });
    }
    App.request("/registry.json", function (e, r) {
      try {
        Settings.registry = JSON.parse(r);
      } catch (e) {
        Settings.registry = {};
      }

      APP_NAMESPACE = Settings.NAMESPACE;
      LANGS = Settings.LANGS;

      App.createNamespace(App.namespace);

      function starter() {
        App.fn.load(function () {
          var init = function () {
            var div = document.createElement("div");
            div.style.display = "none";
            div.className = "x-panel";
            App._root = div;
            App.$("body").dom().appendChild(App._root);
            if (Settings.CONTROLLERS[0])
              App.controller.load(Settings.CONTROLLERS[0]);
          };
          init();
        });
      }

      if (Settings.DEBUG) lo(Settings.API, 0, starter);
      else starter();
    });
  },
});
